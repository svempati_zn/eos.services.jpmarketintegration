﻿using eos.service.JPMarketIntegration.Domain.Models;
using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces;
using Eos.Infrastructure.AppConfiguration;
using Eos.Infrastructure.Data.DataProvider.SqlProvider;
using Microsoft.Extensions.Options;
using System.Data;
using System.Data.SqlClient;

namespace eos.service.JPMarketIntegration.infrastructure.Repositories
{
    public class OcctoRepository : IOcctoRepository
    {        
        private readonly ApplicationConfiguration _appConfiguration;
        private readonly MarketSettings _marketSettings;

        public OcctoRepository(IOptions<ApplicationConfiguration> appConfiguration, IOptions<MarketSettings> marketSettings)
        {
            _appConfiguration = appConfiguration.Value;
            _marketSettings = marketSettings.Value;
        }

        public List<string> GetUtilityFileLogNamesByIsProcessed(int utilityId)
        {
            var fileNames = new List<string>();
            var transactionType = string.Empty;
            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            {
                using (var command = connection.CreateCommand("[TRX].[GetProcessedUtilityFileLogNames_V1]"))
                {
                    command.AddWithValue("@UtilityId", utilityId);

                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                            reader.TryGetString("OriginalFileName", x => fileNames.Add(x));
                    }
                }
            }
            return fileNames;
        }

        public int InsertUtilityFileLog(UtilityFileLogModel utilityFileLog)
        {
            var transactionType = string.Empty;
            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            {
                using (var command = connection.CreateCommand("[TRX].[InsertUtilityFileLog_V1]"))
                {
                    SqlParameter outputParameter;
                    command.AddOutParameter("@UtilityFileLog", SqlDbType.Int, out outputParameter);
                    command.AddWithValue("@UtilityId", utilityFileLog.UtilityId);
                    command.AddWithValue("@OriginalFileName", utilityFileLog.OriginalFileName);
                    command.AddWithValue("@FileName", utilityFileLog.FileName);
                    command.AddWithValue("@IsProcessed", utilityFileLog.IsProcessed);
                    command.AddWithValue("@CreatedBy", _marketSettings.UserId);

                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                    var affectedRowsCount = command.ExecuteNonQuery();
                    if (affectedRowsCount > 0)
                    {
                        utilityFileLog.UtilityFileLogId = Convert.ToInt32(outputParameter.Value);
                    }
                }
            }
            return utilityFileLog.UtilityFileLogId;
        }

        public int InsertUtilityFileDetailLog(UtilityFileLogDetailModel utilityFileLogDetail)
        {
            var transactionType = string.Empty;
            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            {
                using (var command = connection.CreateCommand("[TRX].[InsertUtilityFileLogDetail_V1]"))
                {
                    SqlParameter outputParameter;
                    command.AddOutParameter("@UtilityFileLogDetailId", SqlDbType.Int, out outputParameter);
                    command.AddWithValue("@UtilityFileLogId", utilityFileLogDetail.UtilityFileLogId);
                    command.AddWithValue("@OriginalFileName", utilityFileLogDetail.OriginalFileName);
                    command.AddWithValue("@CreatedBy", _marketSettings.UserId);

                    if (connection.State != ConnectionState.Open)
                        connection.Open();
                    var affectedRowsCount = command.ExecuteNonQuery();
                    if (affectedRowsCount > 0)
                    {
                        utilityFileLogDetail.UtilityFileLogDeailId = Convert.ToInt32(outputParameter.Value);
                    }
                }
            }
            return utilityFileLogDetail.UtilityFileLogDeailId;
        }
        public bool UpdateUtilityFileLog(UtilityFileLogModel utilityFileLog)
        {
            var affectedRowsCount = 0;
            var transactionType = string.Empty;
            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            {
                using (var command = connection.CreateCommand("[TRX].[UpdateUtilityFileLogById_V1]"))
                {
                    command.AddWithValue("@UtilityFileLogId", utilityFileLog.UtilityFileLogId);
                    command.AddWithValue("@FileName", utilityFileLog.FileName);
                    command.AddWithValue("@IsProcessed", utilityFileLog.IsProcessed);
                    command.AddWithValue("@ProcessedDate", utilityFileLog.ProcessedDate);
                    command.AddWithValue("@LastModifiedBy", _marketSettings.UserId);
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    affectedRowsCount = command.ExecuteNonQuery();
                }
            }
            return affectedRowsCount > 0;
        }
        public bool UpdateUtilityFileLogDetail(UtilityFileLogDetailModel utilityFileLogDetail)
        {
            var affectedRowsCount = 0;
            var transactionType = string.Empty;
            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            {
                using (var command = connection.CreateCommand("[TRX].[UpdateUtilityFileLogDetailById_V1]"))
                {
                    command.AddWithValue("@UtilityFileLogDetailId", utilityFileLogDetail.UtilityFileLogDeailId);
                    command.AddWithValue("@FileName", utilityFileLogDetail.FileName);
                    command.AddWithValue("@LastModifiedBy", _marketSettings.UserId);
                    if (connection.State != ConnectionState.Open)
                        connection.Open();

                    affectedRowsCount = command.ExecuteNonQuery();
                }
            }
            return affectedRowsCount > 0;
        }
        public string GetClientDunsByUtilityDuns(string utilityDuns)
        {
            var clientDuns = string.Empty;

            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            {
                using (var command = connection.CreateCommand("[TRX].[GetEdiProviderByUtilityDuns_V1]"))
                {
                    command.AddWithValue("@UtilityDuns", utilityDuns);
                    connection.Open();
                    using (var reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                            reader.TryGetString("ClientDuns", x => clientDuns = x);
                    }
                }
            }
            return clientDuns;
        }
    }
}