﻿using eos.service.JPMarketIntegration.Domain.Models;
using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces;
using Eos.Infrastructure.AppConfiguration;
using Eos.Infrastructure.Data.DataProvider.SqlProvider;
using System.Data;
using Microsoft.Extensions.Options;

namespace eos.service.JPMarketIntegration.infrastructure.Repositories
{
    public class UtilityRepository : IUtilityRepository
    {
        private readonly ApplicationConfiguration _appConfiguration;
        private readonly MarketSettings _marketSettings;

        public UtilityRepository(IOptions<ApplicationConfiguration> appConfiguration, IOptions<MarketSettings> marketSettings)
        {
            _appConfiguration = appConfiguration.Value;
            _marketSettings = marketSettings.Value;           
        }        

        public List<UtilityFileConfiguration> GetAllUtilityFileConfig()
        {
            var utilityFileConfigurationData = new List<UtilityFileConfiguration>();
            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            using (var command = connection.CreateCommand("[COM].[GetUtilityFileConfiguration_V1]"))
            {
                command.AddWithValue("@IsActive", true);
                if (connection.State != ConnectionState.Open)
                    connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var utility = new UtilityFileConfiguration();
                        reader.TryGetInt32("UtilityFileConfigurationId", u => utility.UtilityFileConfigurationId = u);
                        reader.TryGetInt32("UtilityId", u => utility.UtilityId = u);
                        reader.TryGetInt32("MarketId", u => utility.MarketId = u);
                        reader.TryGetString("UtilityName", u => utility.UtilityName = u);
                        reader.TryGetString("Host", u => utility.Host = u);
                        reader.TryGetString("FileListUrl", u => utility.FileListUrl = u);
                        reader.TryGetString("FileDownloadUrl", u => utility.FileDownloadUrl = u);
                        reader.TryGetString("EntityCode", u => utility.EntityCode = u.Trim());
                        reader.TryGetString("UtilityCode", u => utility.UtilityCode = u);
                        utilityFileConfigurationData.Add(utility);
                    }
                }
            }
            return utilityFileConfigurationData;
        }

        public bool UpdateUtilityFileConfiguration(int utilityFileConfigId)
        {
            var affectedRowsCount = 0;

            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            using (var command = connection.CreateCommand("[COM].[UpdateUtilityFileConfiguration_V1]"))
            {
                command.AddWithValue("@UtilityFileConfigurationId", utilityFileConfigId);
                command.AddWithValue("@LastProcessedDate", DateTime.UtcNow);

                if (connection.State != ConnectionState.Open)
                    connection.Open();
                affectedRowsCount = command.ExecuteNonQuery();
            }
            return affectedRowsCount > 0;
        }

        public IEnumerable<Utility> GetUtilities(int? utilityId = null)
        {
            var result = new List<Utility>();

            using (var connection = DbFactory.CreateConnection(_appConfiguration.DbConnectionString))
            using (var command = connection.CreateCommand("[COM].[GetAllUtilities_V1]"))
            {
                command.AddWithValue("@IsActive", true);
                if ((utilityId.HasValue) && (utilityId > 0))
                    command.AddWithValue("@UtilityId", utilityId);
                command.AddWithValue("@LanguageCode", _marketSettings.LanguageCode);
                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (!string.IsNullOrEmpty(reader["UtilityNameDisplay"] as string))
                        {
                            var item = new Utility
                            {
                                UtilityId = reader.GetInt32("UtilityId"),
                                UtilityName = reader.GetString("UtilityName"),
                                UtilityNameDisplay = reader.GetString("UtilityNameDisplay"),
                                UtilityCode = reader.GetString("UtilityCode"),
                                ServiceTypeId = reader.GetInt32("ServiceTypeId"),
                                ISOId = reader.GetInt32("ISOId"),
                                PUCId = reader.GetInt32("PUCId"),
                                MarketId = reader.GetInt32("MarketId"),
                                UtilityDuns = reader.GetString("UtilityDuns"),
                                DefaultDecimalPrecision = reader.GetInt32("DefaultDecimalPrecision"),
                                NameKeyRequired = reader.GetBoolean("NameKeyRequired"),
                                UtilityAccountNumberLength = reader.GetInt16("UtilityAccountNumberLength"),
                                UtilityAccountNumberIsNumeric = reader.GetBoolean("UtilityAccountNumberIsNumeric"),
                                UtilityBillingAccountNumberRequired = reader.GetBoolean("UtilityBillingAccountNumberRequired")
                            };
                            if (!string.IsNullOrEmpty(reader["UtilityState"] as string))
                                item.UtilityState = reader.GetString("UtilityState");

                            reader.TryGetString("ParentCompany", x => item.ParentCompany = x);
                            reader.TryGetBoolean("IsActive", x => item.IsActive = x);
                            reader.TryGetString("UtilityAccountNumberImage", x => item.UtilityAccountNumberImage = x);
                            reader.TryGetString("NameKeyImage", x => item.NameKeyImage = x);
                            reader.TryGetString("UtilityBillingAccountNumberImage", x => item.UtilityBillingAccountNumberImage = x);
                            reader.TryGetString("UtilityAccountNumberName", x => item.UtilityAccountNumberName = x);
                            reader.TryGetString("NameKeyName", x => item.NameKeyName = x);
                            reader.TryGetString("UtilityBillingAccountNumberName", x => item.UtilityBillingAccountNumberName = x);
                            reader.TryGetBoolean("BirthdateRequired", x => item.BirthdateRequired = x);
                            reader.TryGetString("UtilityAccountNumberRegex", x => item.UtilityAccountNumberRegex = x);
                            reader.TryGetString("NameKeyRegex", x => item.NameKeyRegex = x);
                            reader.TryGetString("UtilityBillingAccountNumberRegex", x => item.UtilityBillingAccountNumberRegex = x);
                            reader.TryGetString("UtilityWebSite", x => item.UtilityWebSite = x);
                            reader.TryGetString("UtilityPhone", x => item.UtilityPhone = x);
                            reader.TryGetString("UtilityOutagePhone", x => item.UtilityOutagePhone = x);
                            reader.TryGetString("UtilityEmail", x => item.UtilityEmail = x);
                            if (!string.IsNullOrEmpty(reader["CrmUtilityAccountNumberRegex"] as string))
                                item.CrmUtilityAccountNumberRegex = reader.GetString("CrmUtilityAccountNumberRegex");
                            result.Add(item);
                        }

                    }

                    return result.ToList();
                }
            }
        }
    }
}