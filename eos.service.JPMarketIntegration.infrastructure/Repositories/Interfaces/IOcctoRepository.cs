﻿
using eos.service.JPMarketIntegration.Domain.Models;

namespace eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces
{
	public interface IOcctoRepository
	{
		List<string> GetUtilityFileLogNamesByIsProcessed(int utilityId);

		int InsertUtilityFileLog(UtilityFileLogModel utilityFileLog);
		int InsertUtilityFileDetailLog(UtilityFileLogDetailModel utilityFileLogDetail);
		bool UpdateUtilityFileLog(UtilityFileLogModel utilityFileLog);
		bool UpdateUtilityFileLogDetail(UtilityFileLogDetailModel utilityFileLogDetail);
		string GetClientDunsByUtilityDuns(string utilityDuns);
	}
}