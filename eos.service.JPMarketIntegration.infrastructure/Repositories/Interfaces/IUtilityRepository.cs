﻿using eos.service.JPMarketIntegration.Domain.Models;
using eos.service.JPMarketIntegration.Domain.Models.Lookup;

namespace eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces
{
	public interface IUtilityRepository
	{
		List<UtilityFileConfiguration> GetAllUtilityFileConfig();
		bool UpdateUtilityFileConfiguration(int utilityFileConfigId);
		IEnumerable<Utility> GetUtilities(int? utilityId = null);
	}
}