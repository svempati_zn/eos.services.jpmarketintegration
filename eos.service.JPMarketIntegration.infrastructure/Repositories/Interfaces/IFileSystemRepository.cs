﻿using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using EOS.Infrastructure.VirtualPath;

namespace eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces
{
	public interface IFileSystemRepository
	{
        bool IsSatisfied(string providerName);
        string FormatPath(string path);        
        Stream GetFileStream(string path);
        string CreateFile(string path, byte[] barray);
        IVirtualFile GetFile(string fileName);
    }
}