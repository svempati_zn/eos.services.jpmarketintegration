﻿using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces;
using Eos.Infrastructure.AppConfiguration;
using Eos.Infrastructure.Data.DataProvider.SqlProvider;
using EOS.Infrastructure.VirtualPath;
using Microsoft.Extensions.Options;
using System.Data;

namespace eos.service.JPMarketIntegration.infrastructure.Repositories
{
    public class FileSystemRepository : IFileSystemRepository
    {
        private readonly ApplicationConfiguration _appConfiguration;
        private IVirtualPathProvider _virutualPathProvider;
        private const string ProviderName = "FileSystem";

        public FileSystemRepository(IOptions<ApplicationConfiguration> appConfiguration)
        {
            _appConfiguration = appConfiguration.Value;
        }
        public bool IsSatisfied(string providerName)
        {
            if (!providerName.Equals(ProviderName, System.StringComparison.OrdinalIgnoreCase)) return false;
            _virutualPathProvider = VirtualPathProvider.Open(ProviderName, _appConfiguration.FilesharePath);
            return true;
        }
        public string FormatPath(string path)
        {
            return path;
        }
        public Stream GetFileStream(string path)
        {
            var fileInfo = new FileInfo(path);
            return fileInfo.OpenRead();

        }

        public string CreateFile(string path, byte[] barray)
        {
            var vFile = _virutualPathProvider.CreateFile(FormatPath(path), barray);
            return vFile.VirtualPath;
        }

        public IVirtualFile GetFile(string fileName)
        {
            fileName = fileName.Replace(_virutualPathProvider.RootDirectory.RealPath, "");
            return _virutualPathProvider.GetFile(fileName);
        }
    }
}