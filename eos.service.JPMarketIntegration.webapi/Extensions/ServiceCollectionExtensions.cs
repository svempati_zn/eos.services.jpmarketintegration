﻿//using Eos.Service.JPMarketIntegration.WebApi.Filters;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
//using Eos.Service.JPMarketIntegration.WebApi.Attributes;
//using Swashbuckle.AspNetCore.Filters;
using eos.service.JPMarketIntegration.webapi.Attributes;
using Swashbuckle.AspNetCore.Filters;

namespace eos.service.JPMarketIntegration.webapi.Extensions
{
        public static class ServiceCollectionExtensions
        {
        public static void ConfigureSwagger(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Eos JPMarketIntegration Api", Version = "v1" });
                c.OperationFilter<CustomHeaderSwaggerAttribute>();
                c.ExampleFilters();
                ////var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}-v1.xml";
                ////var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                //// c.IncludeXmlComments(xmlPath);

                //var azureAdBaseEndpoint = string.Format("{0}{1}", Configuration["AzureAd:Instance"], Configuration["AzureAd:TenantId"]);
                //c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                //{
                //    Type = SecuritySchemeType.OAuth2,
                //    Flows = new OpenApiOAuthFlows
                //    {
                //        Implicit = new OpenApiOAuthFlow
                //        {
                //            AuthorizationUrl = new Uri(string.Format("{0}/oauth2/v2.0/authorize", azureAdBaseEndpoint), UriKind.Absolute),
                //            Scopes = new Dictionary<string, string>
                //            {
                //                { Configuration["Swagger:Scope"], "Access to Occto Service API endpoints" },
                //            }
                //        }
                //    }
                //});

                //c.AddSecurityRequirement(new OpenApiSecurityRequirement
                //{
                //    {
                //        new OpenApiSecurityScheme
                //        {
                //            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
                //        },
                //        new[] { Configuration["Swagger:Scope"] }
                //    }
                //});
                //c.TagActionsBy(api =>
                //{
                //    if (api.GroupName != null)
                //    {
                //        return new[] { api.GroupName };
                //    }

                //    var controllerActionDescriptor = api.ActionDescriptor as ControllerActionDescriptor;
                //    if (controllerActionDescriptor != null)
                //    {
                //        return new[] { controllerActionDescriptor.ControllerName };
                //    }

                //    throw new InvalidOperationException("Unable to determine tag for endpoint.");
                //});
                //c.DocInclusionPredicate((name, api) => true);
                //c.DocumentFilter<SwaggerOperationsOrderingFilter>();
            });
            services.AddSwaggerExamples();
        }
    }
}