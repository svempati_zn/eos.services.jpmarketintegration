﻿using eos.service.occto.application.Interfaces;
using eos.service.occto.Domain.Models.Request;
using eos.service.occto.Domain.Models.Response;
using Eos.Infrastructure.AppConfiguration;
using EOS.Infrastructure.Common.Response;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Web.Http;

namespace eos.service.occto.webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OcctoController : ControllerBase
    {
        private readonly IOcctoGatewayProcessor _processor;
        private readonly ApplicationConfiguration _configuration;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="processor"></param>
        public OcctoController(IOcctoGatewayProcessor processor, IOptionsSnapshot<ApplicationConfiguration> configuration)
        {
            this._processor = processor;
            this._configuration = configuration.Value;
        }

        /// <summary>
        /// Get Facility Information
        /// </summary>
        /// <param name="utilityAccountNumber"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetFacilityInfo")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FacilityInformation))]
        public async Task<IActionResult> GetFacilityInformation(FacilityInfoRequest reqObject)
        {
            var response = await _processor.GetFacilityInformation(reqObject);
            if (response == null)
                return BadRequest(new Exception("Request object is null."));

            if (response.Code == ResponseCode.OK)
            {
                return Ok(response);
            }
            return BadRequest(new Exception("response" + response + "request" + Request));
        }

        [HttpPost]
        //[Route("preSwitchReq/lowvoltage")]
        [Route("GetLowVoltagePrelimSwitchRequest")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PriliminarySwitch))]
        public async Task<IActionResult> GetLowVoltagePrelimSwitchRequest(PrelimSwitchLowVoltageRequest obj)
        {
            var response = await _processor.GetLowVoltagePrelimSwitchRequest(obj);
            if (response == null)
                return BadRequest(new Exception("Request object is null."));
            if (response.Code == ResponseCode.OK)
            {
                return Ok(response);
            }
            return BadRequest(new Exception("response" + response + "request" + Request));
        }

        [HttpPost]
        //[Route("preSwitchReq/highvoltage")]
        [Route("GetHighVoltagePrelimSwitchRequest")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Response<PreliminarySwitchHighVoltage>))]
        public async Task<IActionResult> GetHighVoltagePrelimSwitchRequest(PrelimSwitchHighVoltageRequest obj)
        {
            var response = await _processor.GetHighVoltagePrelimSwitchRequest(obj);
            if (response == null)
                return BadRequest(new Exception("Request object is null."));
            if (response.Code == ResponseCode.OK)
            {
                return Ok(response);
            }

            return BadRequest(new Exception("response" + response + "request" + Request));
        }
    }
}