﻿using eos.service.JPMarketIntegration.application.Interfaces;
using Eos.Infrastructure.AppConfiguration;
using EOS.Infrastructure.Common.Response;
using EOS.Infrastructure.Filters;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Web.Http;

namespace eos.service.JPMarketIntegration.webapi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilityUsageController : ControllerBase
    {
        private readonly IOcctoTaskProcessor _processor;
        private readonly ApplicationConfiguration _configuration;


        public UtilityUsageController(IOcctoTaskProcessor processor, IOptionsSnapshot<ApplicationConfiguration> configuration)
        {
            this._processor = processor;
            this._configuration = configuration.Value;
        }


        /// <summary>
        /// Get Utility Files
        /// </summary>
        [HttpGet]
        [LockAction]
        [Route("Import/GetUtilityFiles")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUtilityFiles(CancellationToken token)
        {
            await _processor.GetUtilityFiles(token);
            return Ok(true);
        }      
    }
}