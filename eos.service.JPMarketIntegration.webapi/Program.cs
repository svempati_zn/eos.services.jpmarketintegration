using EOS.Infrastructure.Logging;
using EOS.Infrastructure.Logging.Interfaces;
using eos.service.JPMarketIntegration.webapi.Extensions;
using eos.service.JPMarketIntegration.webapi;
using Eos.Infrastructure.AppConfiguration;
using eos.service.JPMarketIntegration.Domain.Models;



//var builder = WebApplication.CreateBuilder(args);
var builder = EosHostBuilder.CreateEosBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddSingleton<EOS.Infrastructure.Logging.Interfaces.ILogger>(x =>
//LoggerFactory.GetLogger(builder.Configuration["LoggerProviderName"], builder.Configuration["LoggerHost"], builder.Configuration["LoggerPort"], "PreEnrollment -EnrollmentApi", builder.Configuration["EnvironmentName"])
EOS.Infrastructure.Logging.LoggerFactory.GetLogger("gelf.http", "http://external-graylog.zsys.io", "12202", "JP Market Integration", "local")
);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.Configure<MarketSettings>(builder.Configuration.GetSection("MarketSettings"));
builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();
builder.Services.ConfigureSwagger(builder.Configuration);
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
//builder.Services.AddAzureAppConfiguration();
builder.Services.RegisterRepositoriesDI();
builder.Services.RegisterApplicationUseCase();
//string connectionString = default(string);
//builder.Host.ConfigureAppConfiguration(delegate (HostBuilderContext context, IConfigurationBuilder configBuilder)
//{
//    //config.Build();
//    try
//    {
//        var config = configBuilder.Build();
//        //connectionString = "Endpoint=https://navneetappconfignew.azconfig.io;Id=Gv+j-l0-s0:V/QGuA1k7xwUmAuNVKUm;Secret=PP8vvyDJSloiRUv/TzdDcvTgAuk4J/fmYwtok/fqEPw=";//configuration.GetConnectionString("AzureAppConfiguration");
//        configBuilder.AddAzureAppConfiguration(options =>
//        {
//            connectionString = config.GetConnectionString("AzureAppConfiguration");
//            options.Connect(connectionString).Select("*", "Default");
//            //options.Connect("Endpoint=https://r-eos-app-config.azconfig.io;Id=2j77-l6-s0:MYUq2KxZX0oSvvvP31Ul;Secret=p3l8gpN1vjhQ2KuA5Y1Yvds65VH2Gvv1zJ63iyce3Po=");
//            //options.Connect("Endpoint=https://navneetappconfignew.azconfig.io;Id=Gv+j-l0-s0:V/QGuA1k7xwUmAuNVKUm;Secret=PP8vvyDJSloiRUv/TzdDcvTgAuk4J/fmYwtok/fqEPw=");
//            options.ConfigureKeyVault(kv =>
//            {
//                kv.SetCredential(new ClientSecretCredential(config["KeyVault:keyvault_tenantid"], config["KeyVault:keyvault_clientid"], config["KeyVault:keyvault_clientsecret"]));
//            });
//        });
//        //    kv.SetCredential(new ClientSecretCredential($"https://{configuration["KeyVault:Vault"]}.vault.azure.net/", configuration["KeyVault:ClientId"], configuration["KeyVault:ClientSecret"]));
//        configBuilder.Build();
//    }
//    catch (Exception)
//    {
//    }
//});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
