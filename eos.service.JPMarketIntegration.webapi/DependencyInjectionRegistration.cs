﻿using eos.service.JPMarketIntegration.application.Interfaces;
using eos.service.JPMarketIntegration.application.Processors;
using eos.service.JPMarketIntegration.application.Services;
using eos.service.JPMarketIntegration.infrastructure.Repositories;
using eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces;

namespace eos.service.JPMarketIntegration.webapi
{
    public static class DependencyInjectionRegistration
    {
        public static void RegisterRepositoriesDI(this IServiceCollection services)
        {
            services.AddSingleton<IOcctoRepository, OcctoRepository>();
            services.AddSingleton<IUtilityRepository, UtilityRepository>();
            services.AddSingleton<IFileSystemRepository, FileSystemRepository>();
        }


        public static void RegisterApplicationUseCase(this IServiceCollection services)
        {
            services.AddSingleton <IUtilityFileRecieveService, UtilityFileRecieveService>();
            services.AddSingleton<IUtilityFileReceiveProcessor, UtilityFileReceiveProcessor>();
            services.AddSingleton<IUtilityProcessor, UtilityProcessor>();
            services.AddSingleton<IOcctoTaskProcessor, OcctoTaskProcessor>();        
            services.AddSingleton<IOcctoImportProcessor, OcctoImportProcessor>();
          
            //services.AddSingleton<IDataContext , HttpContextAccessor>();
        }
    }
}


