﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eos.service.JPMarketIntegration.Domain.Models
{
    public class MarketBase
    {
        public int MarketId { get; set; }
        public string StateCode { get; set; }
        public string MarketName { get; set; }
        public bool IsActive { get; set; }
        public bool IsZNActive { get; set; }
    }
}
