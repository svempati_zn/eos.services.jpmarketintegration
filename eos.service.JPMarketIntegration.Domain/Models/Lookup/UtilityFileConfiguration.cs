﻿
namespace eos.service.JPMarketIntegration.Domain.Models.Lookup
{
    public class UtilityFileConfiguration
    {
        public int UtilityFileConfigurationId { get; set; }
        public int UtilityId { get; set; }
        public int MarketId { get; set; }
        public string UtilityName { get; set; }
        public string Host { get; set; }
        public string FileListUrl { get; set; }
        public string FileDownloadUrl { get; set; }
        private string _entityCode;
        public string EntityCode
        {
            get { return _entityCode + UtilityCode.Substring(1); }
            set { _entityCode = value; }
        }
        public string UtilityCode { get; set; }
    }
}
