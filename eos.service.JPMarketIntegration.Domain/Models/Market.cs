﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eos.service.JPMarketIntegration.Domain.Models
{
    public class Market : MarketBase
    {
        public virtual ICollection<Utility> Utility { get; set; }
    }
}
