﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eos.service.JPMarketIntegration.Domain.Models
{
    public class Utility : UtilityBase
    {
        public virtual Market Market { get; set; }
        public virtual ICollection<Zone> Zone { get; set; }
    }
}
