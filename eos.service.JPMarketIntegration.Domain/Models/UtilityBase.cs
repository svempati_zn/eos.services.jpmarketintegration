﻿using System;
using System.Collections.Generic;

namespace eos.service.JPMarketIntegration.Domain.Models
{
    public class UtilityBase
    {
        public int UtilityId { get; set; }
        public string UtilityName { get; set; }
        public string UtilityNameDisplay { get; set; }
        public string UtilityCode { get; set; }
        public string ParentCompany { get; set; }
        public int ServiceTypeId { get; set; }
        public int ISOId { get; set; }
        public int PUCId { get; set; }
        public int MarketId { get; set; }
        public string UtilityDuns { get; set; }
        public string UtilityStreetAddress { get; set; }
        public string UtilityCity { get; set; }
        public string UtilityState { get; set; }
        public string UtilityZip { get; set; }
        public string UtilityEmail { get; set; }
        public string UtilityWebSite { get; set; }
        public string UtilityOutagePhone { get; set; }
        public bool IsActive { get; set; }
        public Int16? UtilityAccountNumberLength { get; set; }
        public bool? UtilityAccountNumberIsNumeric { get; set; }
        public int DefaultDecimalPrecision { get; set; }
        public bool NameKeyRequired { get; set; }
        public bool UtilityBillingAccountNumberRequired { get; set; }
        public int[] UtilityIds { get; set; }
        public string UtilityAccountNumberImage { get; set; }
        public string NameKeyImage { get; set; }
        public string UtilityBillingAccountNumberImage { get; set; }
        public string UtilityAccountNumberName { get; set; }
        public string NameKeyName { get; set; }
        public string UtilityBillingAccountNumberName { get; set; }
        public bool BirthdateRequired { get; set; }
        public string UtilityAccountNumberRegex { get; set; }
        public string NameKeyRegex { get; set; }
        public string UtilityBillingAccountNumberRegex { get; set; }
        public string UtilityPhone { get; set; }
        public string CrmUtilityAccountNumberRegex { get; set; }
    }
}
