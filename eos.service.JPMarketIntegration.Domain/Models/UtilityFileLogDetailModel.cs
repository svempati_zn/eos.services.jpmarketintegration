﻿
namespace eos.service.JPMarketIntegration.Domain.Models
{
    public class UtilityFileLogDetailModel
    {
        public int UtilityFileLogDeailId { get; set; }
        public int UtilityFileLogId { get; set; }
        public string FileName { get; set; }
        public string OriginalFileName { get; set; }

    }
}
