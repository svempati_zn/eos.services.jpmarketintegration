﻿namespace eos.service.JPMarketIntegration.Domain.Models
{
    public class MarketSettings
    {       
        public string FilesharePathProvider { get; set; }        
        public string LanguageCode { get; set; }       
        public int UserId { get; set; }
   
    }
}
