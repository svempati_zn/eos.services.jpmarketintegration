﻿
namespace eos.service.JPMarketIntegration.Domain.Models
{
	public class UtilityFileLogModel
	{
		public int UtilityFileLogId { get; set; }
		public int UtilityId { get; set; }
		public string UtilityName { get; set; }
		public string OriginalFileName { get; set; }
		public string FileName { get; set; }
		public bool IsProcessed { get; set; }
		public DateTime ProcessedDate { get; set; }
	}
}
