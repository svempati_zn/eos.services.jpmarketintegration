﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eos.service.JPMarketIntegration.Domain.Models
{
    public class ZoneBase
    {
        public int ZoneId { get; set; }
        public int UtilityId { get; set; }
        public int ServiceTypeId { get; set; }
        public string LoadZone { get; set; }
        public string SubZone { get; set; }
        public string ZoneName { get; set; }
        public bool IsActive { get; set; }
    }
}
