﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eos.service.JPMarketIntegration.Domain.Enums
{
    public enum VirtualPathType
    {
        FileSystem = 1,
        InMemory = 2,
        BrickFtp = 3,
        AzureBlob = 4,
        FTP = 5,
        SFTP = 6,
    }
}