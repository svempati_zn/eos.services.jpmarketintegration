﻿using eos.service.JPMarketIntegration.application.Interfaces;
using eos.service.JPMarketIntegration.Domain.Enums;
using eos.service.JPMarketIntegration.Domain.Models;
using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using Eos.Infrastructure.AppConfiguration;
using EOS.Infrastructure.Common.Response;
using EOS.Infrastructure.VirtualPath;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Extensions.Options;
using System.IO.Compression;
using System.Net;
using System.Net.Mime;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;

namespace eos.service.JPMarketIntegration.application.Services
{
    public class UtilityFileRecieveService : IUtilityFileRecieveService
    {
        private X509Certificate2 _certificate;
        private readonly ApplicationConfiguration _appConfiguration;
        private readonly MarketSettings _marketSettings;

        public UtilityFileRecieveService(IOptions<ApplicationConfiguration> appConfiguration, IOptions<MarketSettings> marketSettings)
        { 
            _appConfiguration = appConfiguration.Value;
            _marketSettings = marketSettings.Value;
        }

        public void SetOcctoThumbPrint(string occtoClientCode)
        {
            var certStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            certStore.Open(OpenFlags.ReadOnly);
            if (certStore.Certificates.Count <= 0) return;

            var certinfo = Convert.FromBase64String(_appConfiguration.testOcctoKvReference.ToString());
            _certificate = new X509Certificate2(certinfo);
            certStore.Close();
        }

        /// <summary>
        /// Gets the Http Web response response from five connectivityUrl and reads Data Stream
        /// </summary>
        /// <param name="connectivityUrl">Url to which request must be sent</param>
        /// <param name="hostUrl">Host of Connectivity Url</param>
        /// <returns>returns Stream Resonse</returns>
        public Response<string> GetHttpWebStreamReponse(string connectivityUrl, string hostUrl, string occtoClientCode)
        {
            string streamResponseFromServer = null;
            int? thirdPartyRequestLogId = null;
            try
            {
                //ServicePointManager.Expect100Continue = true;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                //       | SecurityProtocolType.Tls11
                //       | SecurityProtocolType.Tls12
                //       | SecurityProtocolType.Ssl3;
                var request =
                (HttpWebRequest)WebRequest.Create(connectivityUrl);
                request.Method = "GET";
                request.Host = hostUrl;
                request.Headers.Add("SOAPAction", connectivityUrl);

                SetOcctoThumbPrint(occtoClientCode);
                request.ClientCertificates.Add(_certificate);
                //thirdPartyRequestLogId= _repository.AddThirdPartyRequestLog(0, $"connectivityUrl: {connectivityUrl} , hostUrl:{hostUrl}", RequestTypeConstants.UsageFilesWebResponse);
                var response = (HttpWebResponse)request.GetResponse();

                var dataStream = response.GetResponseStream();
                if (dataStream == null) return new Response<string>(null, ResponseCode.BadRequest);
                using (var reader = new StreamReader(dataStream))
                {
                    streamResponseFromServer = reader.ReadToEnd();
                }
                //var thirdPartyRequestLog = new ThirdPartyRequestLog
                //{
                //    ThirdPartyRequestLogId = thirdPartyRequestLogId.Value,
                //    IsException = false,
                //    ResponseJson= streamResponseFromServer.ToString()
                //};
                //_repository.UpdateThirdPartyResponse(thirdPartyRequestLog);
            }
            catch (Exception ex)
            {
                //var thirdPartyRequestErrorLog = new ThirdPartyRequestLog
                //{
                //    ThirdPartyRequestLogId = thirdPartyRequestLogId.Value,
                //    IsException = true,
                //    ErrorMessage = ThirdPartyRequestLogError.ErrorMessage.FormatMessage(ex.Message, ex).Message
                //};
                //_repository.UpdateThirdPartyResponse(thirdPartyRequestErrorLog);
                return new Response<string>(streamResponseFromServer, ResponseCode.BadRequest, new[] { new ResponseMessage("", ex.Message, Severity.Exception) });
            }


            return new Response<string>(streamResponseFromServer);
        }

        /// <summary>
        /// Downloads file from given FilePath using WebClient
        /// </summary>
        /// <param name="fileName">file Name which has to be retrieved from server</param>
        /// <param name="filePath">Path to which File has to be saved</param>
        /// <param name="utilityFileDetails">Complete Utility details for Download and List Urls</param>
        /// <returns>Returns Path on whic File saved</returns>
        public Response<string> DownloadFile(string fileName, string filePath, UtilityFileConfiguration utilityFileDetails, IVirtualPathProvider _virutualPathProvider)
        {
            try
            {
                // Setting OcctoThumbPrint by OcctoClientCode from Tenant
                SetOcctoThumbPrint(utilityFileDetails.EntityCode);
                using (var client = new UtilityWebClient(utilityFileDetails.Host, utilityFileDetails.FileListUrl, _certificate))
                {
                    object[] args = new object[] { fileName, utilityFileDetails.EntityCode };
                    if (_marketSettings.FilesharePathProvider == Enum.GetName(typeof(VirtualPathType), VirtualPathType.AzureBlob))
                    {
                        using (MemoryStream stream = new MemoryStream(client.DownloadData(string.Format(utilityFileDetails.FileDownloadUrl, args))))
                        {
                            byte[] bytes = stream.ToArray();
                            if (_virutualPathProvider.FileExists(FormatPath(Path.Combine(filePath, fileName))))
                            {
                                var file = _virutualPathProvider.GetFile(FormatPath(Path.Combine(filePath, fileName)));
                                file.Delete();
                            }
                            _virutualPathProvider.CreateFile(FormatPath(Path.Combine(filePath, fileName)), bytes);
                        }
                    }
                    else
                        client.DownloadFile(string.Format(utilityFileDetails.FileDownloadUrl, args), Path.Combine(filePath, fileName));
                    return new Response<string>(Path.Combine(filePath, fileName));
                }
            }
            catch (Exception ex)
            {
                return new Response<string>(null, ResponseCode.BadGateway, new[] { new ResponseMessage("", ex.Message, Severity.Exception) });
            }
        }
        public string FormatPath(string path)
        {
            return path.Replace(@"\", @"/").Replace(@"\", @"/").Replace(@"\", @"/").Replace("\\", "/").Replace("//", "/").Replace("\\", "/").
                Replace("//", "/").Replace("\\", "/").Replace("//", "/").Replace("\\", "/").Replace("//", "/").Replace("\\", "/").Replace("//", "/");
        }

        /// <summary>
        /// Extract zipped file and saves contents to destination
        /// </summary>
        /// <param name="fileName">Zipped file name</param>
        /// <param name="sourceFilePath">Path of Zipped file</param>
        /// <param name="destinationPath">path to which extracted files has to be saved</param>
        /// <returns></returns>
        public Response<IDictionary<string, string>> UnZip(string fileName, string sourceFilePath, string destinationPath, IVirtualPathProvider _virutualPathProvider)
        {
            try
            {
                if (_marketSettings.FilesharePathProvider.Equals(Enum.GetName(typeof(VirtualPathType), VirtualPathType.AzureBlob)))
                {
                    if (!_virutualPathProvider.FileExists(FormatPath(sourceFilePath)))
                        return new Response<IDictionary<string, string>>(null);

                    var file = _virutualPathProvider.GetFile(FormatPath(sourceFilePath));

                    var files = new Dictionary<string, string>();

                    using (var s = new ZipInputStream(file.OpenRead()))
                    {
                        int offset = 0;
                        ZipEntry zipEntry;
                        while ((zipEntry = s.GetNextEntry()) != null)
                        {
                            var unZipFileName = zipEntry.Name;

                            if (!string.IsNullOrEmpty(unZipFileName))
                            {
                                using (MemoryStream mStream = new MemoryStream())
                                {
                                    long size = zipEntry.Size > 0 ? zipEntry.Size : 1024 * 10;
                                    Byte[] data = new Byte[size];

                                    while (true)
                                    {
                                        size = s.Read(data, offset, data.Length);
                                        if (size > 0)
                                        {
                                            mStream.Write(data, offset, (int)size);
                                            //offset += Convert.ToInt32(size);
                                        }
                                        else break;
                                    }
                                    if (_virutualPathProvider.FileExists(FormatPath(Path.Combine(destinationPath, unZipFileName))))
                                    {
                                        var existFile = _virutualPathProvider.GetFile(FormatPath(Path.Combine(destinationPath, unZipFileName)));
                                        existFile.Delete();
                                    }
                                    _virutualPathProvider.CreateFile(FormatPath(Path.Combine(destinationPath, unZipFileName)), mStream.ToArray());
                                    files.Add(unZipFileName, FormatPath(Path.Combine(destinationPath, unZipFileName)));
                                }

                            }
                        }
                    }
                    return new Response<IDictionary<string, string>>(files);
                }
                else
                {
                    var unZipFileName = fileName.ToUpper().Replace(".ZIP", "");
                    if (!File.Exists(sourceFilePath))
                        return new Response<IDictionary<string, string>>(null);

                    var files = new Dictionary<string, string>();

                    using (var archive = System.IO.Compression.ZipFile.OpenRead(sourceFilePath))
                    {
                        foreach (var entry in archive.Entries.Where(entry => !string.IsNullOrEmpty(entry.Name)))
                        {
                            if (File.Exists(Path.Combine(destinationPath, entry.Name)))
                                return new Response<IDictionary<string, string>>(null);

                            entry.ExtractToFile(Path.Combine(destinationPath, entry.Name), true);
                            files.Add(entry.Name, Path.Combine(destinationPath, entry.Name));
                        }
                    }
                    return new Response<IDictionary<string, string>>(files);
                }
            }
            catch (Exception ex)
            {
                return new Response<IDictionary<string, string>>(null, ResponseCode.BadRequest, new[] { new ResponseMessage("", ex.Message, Severity.Exception), });
            }
        }

        private class UtilityWebClient : WebClient
        {
            private readonly string _hostUrl;
            private readonly string _listUrl;
            private readonly X509Certificate2 _certificate;
            internal UtilityWebClient(string hostUrl, string listUrl, X509Certificate2 certificate)
            {
                _hostUrl = hostUrl;
                _listUrl = listUrl;
                _certificate = certificate;
            }
            protected override WebRequest GetWebRequest(Uri address)
            {
                var cookieJar = new CookieContainer();

                HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(address);
                if (request == null) return null;
                request.ClientCertificates.Add(_certificate);
                request.Host = _hostUrl;
                request.CookieContainer = cookieJar;
                request.Referer = _listUrl;
                return request;
            }
        }
    }
}