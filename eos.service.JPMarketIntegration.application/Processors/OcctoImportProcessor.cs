﻿using eos.service.JPMarketIntegration.application.Interfaces;
using eos.service.JPMarketIntegration.Domain.Models;
using eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces;
using EOS.Infrastructure.Logging.Interfaces;

namespace eos.service.JPMarketIntegration.application.Processors
{
    public class OcctoImportProcessor : IOcctoImportProcessor
    {
        private readonly IOcctoRepository _occtoRepository;
        private readonly ILogger _logger;
        public OcctoImportProcessor(ILogger logger, IOcctoRepository occtoRepository)
        {
             _logger = logger;
            _occtoRepository = occtoRepository;
        }
        public List<string> GetUtilityFileLogNamesByIsProcessed(int utilityId)
        {
            return _occtoRepository.GetUtilityFileLogNamesByIsProcessed(utilityId);
        }

        public int InsertUtilityFileLog(UtilityFileLogModel utilityFileLogModel)
        {
            return _occtoRepository.InsertUtilityFileLog(utilityFileLogModel);
        }

        public int InsertUtilityFileLogDetail(UtilityFileLogDetailModel utilityFileLogDetailModel)
        {
            return _occtoRepository.InsertUtilityFileDetailLog(utilityFileLogDetailModel);
        }

        public bool UpdateUtilityFileLog(UtilityFileLogModel utilityFileLogModel)
        {
            return _occtoRepository.UpdateUtilityFileLog(utilityFileLogModel);
        }
        public bool UpdateUtilityFileLogDetail(UtilityFileLogDetailModel utilityFileLogDetailModel)
        {
            return _occtoRepository.UpdateUtilityFileLogDetail(utilityFileLogDetailModel);
        }

        public string GetClientDunsByUtilityDuns(string utilityDuns)
        {
            if (string.IsNullOrEmpty(utilityDuns)) return string.Empty;
            return _occtoRepository.GetClientDunsByUtilityDuns(utilityDuns);
        }

    }
}
