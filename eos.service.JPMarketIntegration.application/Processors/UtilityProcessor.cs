﻿using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using eos.service.JPMarketIntegration.application.Interfaces;
using eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces;
using EOS.Infrastructure.Common.Response;

namespace eos.service.JPMarketIntegration.application.Processors
{
    public class UtilityProcessor : IUtilityProcessor
    {
        private readonly IUtilityRepository _utilityRepository;

        public UtilityProcessor(IUtilityRepository utilityRepository)
        {
            _utilityRepository = utilityRepository;
        }

        public List<UtilityFileConfiguration> GetAllUtilityFileConfigData()
        {
            return _utilityRepository.GetAllUtilityFileConfig();
        }

        public Response<bool> UpdateUtilityFileConfiguration(int utilityFileConfigId)
        {
            var response = _utilityRepository.UpdateUtilityFileConfiguration(utilityFileConfigId);
            return response ? new Response<bool>(true) : new Response<bool>(false, ResponseCode.NotModified);
        }

        public string GetUtilityDunsByUtilityId(int? utilityId)
        {
            var utility = _utilityRepository.GetUtilities(utilityId);
            return utility?.FirstOrDefault()?.UtilityDuns;
        }
    }
}
