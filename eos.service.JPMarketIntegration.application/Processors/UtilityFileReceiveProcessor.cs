﻿using eos.service.JPMarketIntegration.application.Interfaces;
using eos.service.JPMarketIntegration.application.Services;
using eos.service.JPMarketIntegration.Domain.Enums;
using eos.service.JPMarketIntegration.Domain.Models;
using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using eos.service.JPMarketIntegration.infrastructure.Repositories.interfaces;
using Eos.Infrastructure.AppConfiguration;
using EOS.Infrastructure.Common.Extensions;
using EOS.Infrastructure.Common.Response;
using EOS.Infrastructure.Logging.Interfaces;
using EOS.Infrastructure.VirtualPath;
using Microsoft.Extensions.Options;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace eos.service.JPMarketIntegration.application.Processors
{
    public class UtilityFileReceiveProcessor : IUtilityFileReceiveProcessor
    {
        private const string PageRegex = @"[^,]+";
        private const string FileRegex = @"rfilename([0-9]*):""(.*?)""";

        private const string Archive = "\\Archive";
        private const string Zipped = "\\Zipped";
        private const string Unzipped = "\\Unzipped";

        private readonly ILogger _logger;
        private readonly IUtilityFileRecieveService _utilityFileRecieveService;
        private readonly IUtilityProcessor _utilityProcessor;
        private readonly IOcctoImportProcessor _occtoImportProcessor;
        private IVirtualPathProvider _sftpProvider;
        private IVirtualPathProvider _fileShareProvider;
        private string _archiveLocation;
        private IVirtualDirectory _sftpDestination;
        private readonly IFileSystemRepository _fileSystemRepository;
        private readonly ApplicationConfiguration _appConfiguration;
        private readonly MarketSettings _marketSettings;

        public UtilityFileReceiveProcessor(ILogger logger, IUtilityProcessor utilityProcessor, IUtilityFileRecieveService utilityFileRecieveService, IOcctoImportProcessor occtoImportProcessor, IEnumerable<IFileSystemRepository> fileSystemRepository, IOptions<ApplicationConfiguration> appConfiguration, IOptions<MarketSettings> marketSettings)
        {
            _logger = logger;
            _utilityProcessor = utilityProcessor;
            _utilityFileRecieveService = utilityFileRecieveService;
            _occtoImportProcessor = occtoImportProcessor;          
            _appConfiguration = appConfiguration.Value;
            _marketSettings = marketSettings.Value;
            _fileSystemRepository = fileSystemRepository.FirstOrDefault(x => x.IsSatisfied(_marketSettings.FilesharePathProvider));
        }

        public async Task Execute()
        {
            var fileShareConnection = _appConfiguration.BlobStorageConnectionString;
            _fileShareProvider = VirtualPathProvider.Open(_marketSettings.FilesharePathProvider, fileShareConnection);

            var utilityDestination = _appConfiguration.BlobContainerName;
            //CreateDirectoryIfNotExists(utilityDestination);

            _archiveLocation = $"{utilityDestination}";
            //CreateDirectoryIfNotExists(_archiveLocation);

            var utilities = _utilityProcessor.GetAllUtilityFileConfigData();
            _logger.Information("Utilities: " + utilities.ToJson());

            if (utilities.Any())
                utilities.ForEach(ExecuteUtilityProcess);
        }
        private void ExecuteUtilityProcess(UtilityFileConfiguration utility)
        {
            var response = _utilityFileRecieveService.GetHttpWebStreamReponse(string.Format(utility.FileListUrl, utility.EntityCode), utility.Host, utility.EntityCode);

            if (response.Code != ResponseCode.OK)
            {
                _logger.Error(response.Messages?.Select(current => current?.Message).Aggregate((current, next) => $"{current}, {next}"),
                    $"Error executing GetHttpWebStreamReponse from Url: {utility.FileListUrl} and Host: {utility.Host}");
                return;
            }

            _utilityProcessor.UpdateUtilityFileConfiguration(utility.UtilityFileConfigurationId);

            var responseFromServer = response.Model;
            _logger.Information("Utilities Server response" + response.Model.ToJson());
            try
            {
                //CreateFolderStructureForUtility(utility.UtilityName);

                var fileList = GetFileNamesFromHtmlResponse(responseFromServer);

                var processedFiles = _occtoImportProcessor.GetUtilityFileLogNamesByIsProcessed(utility.UtilityId).Select(x => Path.GetFileNameWithoutExtension(x));

                fileList.ForEach(file =>
                {
                    if (!processedFiles.Contains(Path.GetFileNameWithoutExtension(file)) && file.IndexOf("W5", StringComparison.OrdinalIgnoreCase) > -1)
                    {
                        var fileResponse = _utilityFileRecieveService.GetHttpWebStreamReponse(string.Format(utility.FileDownloadUrl, file, utility.EntityCode), utility.Host, utility.EntityCode);
                        _logger.Information("Utilities File Response" + fileResponse.Model.ToJson());
                        if (fileResponse.Code == ResponseCode.OK && fileResponse.Model != null)
                            ExecuteFileDownloadProcess(file, utility);
                    }
                });
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"Could not execute process for {utility.UtilityName} at {DateTime.UtcNow.ToString(CultureInfo.InvariantCulture)}");
            }
        }

        private void ExecuteFileDownloadProcess(string receivedFileName, UtilityFileConfiguration utility)
        {
            var utilityFileLog = new UtilityFileLogModel()
            {
                UtilityId = utility.UtilityId,
                UtilityName = utility.UtilityName,
                OriginalFileName = receivedFileName,
                IsProcessed = false
            };

            var downloadedFilePath = DownloadFile(receivedFileName, utility);

            if (downloadedFilePath == null) return;

            //ProcessFile(downloadedFilePath, utilityFileLog);
        }

        private string DownloadFile(string receivedFileName, UtilityFileConfiguration utility)
        {
            var rootFilePath = $"{_archiveLocation}\\{utility.UtilityName}";

            var fileExtension = Path.GetExtension(receivedFileName);

            var downloadablePath = string.Empty;

            if (fileExtension.Equals(".zip")) downloadablePath = $"{rootFilePath}{Zipped}"; //Zipped files to be downloaded in zipped file location
            else downloadablePath = $"{rootFilePath}{Unzipped}";

            var downloadedResponse = _utilityFileRecieveService.DownloadFile(receivedFileName, downloadablePath, utility, _fileShareProvider);

            if (downloadedResponse.Code != ResponseCode.OK)
            {
                _logger.Error(downloadedResponse.Messages?
                    .Select(current => current?.Message).Aggregate((current, next) => $"{current}, {next}"),
                    $"Error downloading file from Url: {string.Format(utility.FileDownloadUrl, receivedFileName)} and Host: {utility.Host}");
                return null;
            }
            _logger.Information("Utilities File Download Response" + downloadedResponse.Model.ToJson());
            return downloadedResponse.Model;
        }

        private void ProcessFile(string filePath, UtilityFileLogModel utilityFileLog)
        {
            var utilityRootPath = Path.Combine(_archiveLocation, utilityFileLog.UtilityName);

            var extension = Path.GetExtension(filePath);

            if (!extension.Equals(".zip") && !extension.Equals(".xml") && !extension.Equals(".csv")) return;

            if (extension.Equals(".zip"))
            {
                var unZipFileLocation = "";
                if (_marketSettings.FilesharePathProvider == Enum.GetName(typeof(VirtualPathType), VirtualPathType.AzureBlob))
                {
                    unZipFileLocation = $"{utilityRootPath}{Unzipped}";
                }
                else
                    unZipFileLocation = $"{_fileShareProvider.RootDirectory.RealPath}{utilityRootPath}{Unzipped}";

                var extractedResult = ExtractW5Files(filePath, utilityFileLog.OriginalFileName, unZipFileLocation);
                if (extractedResult == null) return;
                _logger.Information("Utilities Extracted Files" + extractedResult.ToJson());
                extractedResult
                    .ToList()
                    .ForEach(unzipModel =>
                    {
                        utilityFileLog.OriginalFileName = unzipModel.Key;
                        InsertUtilityFileLogAndMoveToSftp(utilityFileLog, $"{utilityRootPath}{Unzipped}");
                    });
            }

            else
            {
                var file = _fileShareProvider.GetFile(Path.Combine($"{utilityRootPath}{Unzipped}", utilityFileLog.OriginalFileName));

                InsertUtilityFileLogAndMoveToSftp(utilityFileLog, $"{utilityRootPath}{Unzipped}");
            }
        }

        private IDictionary<string, string> ExtractW5Files(string downloadedFilePath, string receivedFileName, string unZipFileLocation)
        {
            var unZipResult = _utilityFileRecieveService.UnZip(receivedFileName, downloadedFilePath, unZipFileLocation, _fileShareProvider);
            if (unZipResult.Code != ResponseCode.OK)
            {
                _logger.Error($"Received Exception(s): {unZipResult.Messages?.Select(current => current?.Message).Aggregate((current, next) => $"{current}, {next}")}", $"Could not Unzip file: {receivedFileName}");
                return null;
            }
            return unZipResult.Model;
        }

        private void InsertUtilityFileLogAndMoveToSftp(UtilityFileLogModel utilityFileLog, string path)
        {
            utilityFileLog.UtilityFileLogId = _occtoImportProcessor.InsertUtilityFileLog(utilityFileLog);
            var utilityFileLogDetails = SplitXmlUsageFile(utilityFileLog, path);
            MoveFileToSftp(utilityFileLog, path, utilityFileLogDetails);
        }

        private UtilityFileLogDetailModel[] SplitXmlUsageFile(UtilityFileLogModel utilityFileLog, string path)
        {
            List<UtilityFileLogDetailModel> utilityFilelLogDetail = new List<UtilityFileLogDetailModel>();
            var takeElements = 1;
            var sourceFile = "";
            var destPath = "";
            if (_marketSettings.FilesharePathProvider == Enum.GetName(typeof(VirtualPathType), VirtualPathType.AzureBlob))
            {
                sourceFile = _fileSystemRepository.FormatPath($"{path}//{utilityFileLog.OriginalFileName}");
                destPath = _fileSystemRepository.FormatPath($"{path}");
            }
            else
            {

                sourceFile = $"{_fileShareProvider.RootDirectory.RealPath}{path}//{utilityFileLog.OriginalFileName}";
                destPath = $"{_fileShareProvider.RootDirectory.RealPath}{path}";
            }
            var sourceFileStream = _fileSystemRepository.GetFileStream(sourceFile);

            XElement xml = XElement.Load(sourceFileStream);


            // Child elements from source file to split by.
            var childNodes = xml.Descendants("JPMR00010");

            // This is the total number of elements to be sliced up into 
            // separate files.
            int cnt = childNodes.Count();
            if (cnt > takeElements)
            {
                var tempSourceFileStream = _fileSystemRepository.GetFileStream(sourceFile);
                XElement xmlTemp = XElement.Load(tempSourceFileStream);
                var skip = 0;
                var take = takeElements;
                var fileno = 0;

                // Split elements into chunks and save to disk.
                while (skip < cnt)
                {
                    xmlTemp.Element("JPMGRP").Element("JPTRM").Element("JPM00010").Elements("JPMR00010").ToList().Remove();
                    // Extract portion of the xml elements.
                    var c1 = childNodes
                                .Skip(skip)
                                .Take(take);

                    // Setup number of elements to skip on next iteration.
                    skip += take;
                    // File sequence no for split file.
                    fileno += 1;
                    // Filename for split file.
                    var fileName = String.Format(Path.GetFileNameWithoutExtension(utilityFileLog.OriginalFileName) + "_split_{0}.xml", fileno);
                    // Create a partial xml document.
                    // XElement frag = new XElement(rootElement, c1);
                    xmlTemp.Element("JPMGRP").Element("JPTRM").Element("JPM00010").Add(c1);
                    // Save to disk.
                    var tempStream = new MemoryStream();
                    xmlTemp.Save(tempStream);
                    _fileSystemRepository.CreateFile(destPath + "//" + fileName, tempStream.ToArray());
                    //xmlTemp.Save(destPath + "//" + fileName);
                    var utilityFileLogDetailId = _occtoImportProcessor.InsertUtilityFileLogDetail(new UtilityFileLogDetailModel
                    {
                        OriginalFileName = fileName,
                        UtilityFileLogId = utilityFileLog.UtilityFileLogId
                    });
                    utilityFilelLogDetail.Add(new UtilityFileLogDetailModel
                    {
                        UtilityFileLogId = utilityFileLog.UtilityFileLogId,
                        UtilityFileLogDeailId = utilityFileLogDetailId,
                        OriginalFileName = fileName
                    });
                }
            }
            else
            {
                var utilityFileLogDetailId = _occtoImportProcessor.InsertUtilityFileLogDetail(new UtilityFileLogDetailModel
                {
                    OriginalFileName = utilityFileLog.OriginalFileName,
                    UtilityFileLogId = utilityFileLog.UtilityFileLogId
                });
                utilityFilelLogDetail.Add(new UtilityFileLogDetailModel
                {
                    UtilityFileLogId = utilityFileLog.UtilityFileLogId,
                    UtilityFileLogDeailId = utilityFileLogDetailId,
                    OriginalFileName = utilityFileLog.OriginalFileName
                });
            }
            return utilityFilelLogDetail.ToArray();
        }


        private void MoveFileToSftp(UtilityFileLogModel utilityFileLog, string filePath, UtilityFileLogDetailModel[] utilityFileLogDetails)
        {
            _logger.Information("Utility File Path" + utilityFileLog.OriginalFileName.ToJson());

            var utilityDuns = _utilityProcessor.GetUtilityDunsByUtilityId(utilityFileLog.UtilityId);
            var clientDuns = _occtoImportProcessor.GetClientDunsByUtilityDuns(utilityDuns);

            foreach (var fileLogDetail in utilityFileLogDetails)
            {
                var file = _fileSystemRepository.GetFile(Path.Combine(filePath, fileLogDetail.OriginalFileName));

                var renamedFileName = string.Empty;
                if (utilityFileLog.OriginalFileName.Substring(2, 4).Equals("1220"))
                    renamedFileName = $"ZW5_{utilityDuns}_{clientDuns}_{fileLogDetail.OriginalFileName}";
                else if (utilityFileLog.OriginalFileName.Substring(2, 4).Equals("1320"))
                    renamedFileName = $"ZMX_{utilityDuns}_{clientDuns}_{fileLogDetail.OriginalFileName}";
                if (_sftpDestination.FileExists(renamedFileName))
                {
                    var sfile = _sftpDestination.Files.Where(x => x.Name == renamedFileName).FirstOrDefault();
                    sfile.Delete();
                }
                file.CopyTo(_sftpDestination, renamedFileName);
                fileLogDetail.FileName = renamedFileName;
                var isUpdatedDetail = _occtoImportProcessor.UpdateUtilityFileLogDetail(fileLogDetail);
                if (!isUpdatedDetail)
                    _logger.Error($"Could not update UtilityFileLogDetail table with Renamed File with UtilityFileLogDetailId: {utilityFileLog.UtilityFileLogId},  original fileName: {utilityFileLog.OriginalFileName} and Renamed: {renamedFileName}");
            }

            utilityFileLog.ProcessedDate = DateTime.UtcNow;
            utilityFileLog.IsProcessed = true;

            var isUpdated = _occtoImportProcessor.UpdateUtilityFileLog(utilityFileLog);

            if (!isUpdated)
                _logger.Error($"Could not update UtilityFileLog table with  UtilityFileLogId: {utilityFileLog.UtilityFileLogId}");
        }


        private static List<string> GetFileNamesFromHtmlResponse(string response)
        {
            // 1.
            // Find all matches in file.
            var m1 = Regex.Matches(response, PageRegex,
                    RegexOptions.Singleline);

            // 2.
            // Loop over each match.
            return m1.Cast<Match>().Select(m => Regex.Match(m.ToString(), FileRegex).Groups[2].ToString()).Where(value => value.Length > 0).ToList();
        }

        private void CreateFolderStructureForUtility(string utilityName)
        {
            var specificUtilityDirectory = $"/{_archiveLocation}/{utilityName}";
            CreateDirectoryIfNotExists(specificUtilityDirectory);

            var zippedFileLocation = $"/{specificUtilityDirectory}{Zipped}";
            CreateDirectoryIfNotExists(zippedFileLocation);

            var unZippedFileLocation = $"/{specificUtilityDirectory}{Unzipped}";
            CreateDirectoryIfNotExists(unZippedFileLocation);
        }
        private void CreateDirectoryIfNotExists(string path)
        {
            if (!_fileShareProvider.DirectoryExists(path))
                _fileShareProvider.CreateDirectory(path);
        }
    }
}
