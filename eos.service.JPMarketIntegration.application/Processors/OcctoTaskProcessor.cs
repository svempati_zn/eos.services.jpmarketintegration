﻿using eos.service.JPMarketIntegration.application.Interfaces;
using EOS.Infrastructure.Logging.Interfaces;

namespace eos.service.JPMarketIntegration.application.Processors
{
    public class OcctoTaskProcessor : IOcctoTaskProcessor
    {
        private readonly IUtilityFileReceiveProcessor _utilityFileReceiveProcessor;
        private readonly ILogger _logger;
        public OcctoTaskProcessor(ILogger logger, IUtilityFileReceiveProcessor utilityFileReceiveProcessor)
        {
             _logger = logger;
            _utilityFileReceiveProcessor = utilityFileReceiveProcessor;
        }
        public async Task GetUtilityFiles(CancellationToken token)
        {
             await _utilityFileReceiveProcessor.Execute();
        }

    }
}
