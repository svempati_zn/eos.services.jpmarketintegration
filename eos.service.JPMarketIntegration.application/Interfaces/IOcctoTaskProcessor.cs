﻿
namespace eos.service.JPMarketIntegration.application.Interfaces
{
    public interface IOcctoTaskProcessor
    {
        Task GetUtilityFiles(CancellationToken token);
    }
}