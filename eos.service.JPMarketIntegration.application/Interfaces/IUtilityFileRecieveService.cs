﻿using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using EOS.Infrastructure.Common.Response;
using EOS.Infrastructure.VirtualPath;

namespace eos.service.JPMarketIntegration.application.Interfaces
{
    public interface IUtilityFileRecieveService
    {
        Response<string> GetHttpWebStreamReponse(string connectivityUrl, string hostUrl, string occtoClientCode);

        Response<string> DownloadFile(string fileName, string filePath, UtilityFileConfiguration utilityFileDetails, IVirtualPathProvider _virutualPathProvider);
        Response<IDictionary<string, string>> UnZip(string fileName, string sourceFilePath, string destinationPath, IVirtualPathProvider _virutualPathProvider);
    }
}