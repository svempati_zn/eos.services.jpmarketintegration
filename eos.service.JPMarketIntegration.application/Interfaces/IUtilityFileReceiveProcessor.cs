﻿namespace eos.service.JPMarketIntegration.application.Interfaces
{
    public interface IUtilityFileReceiveProcessor
    {
        Task Execute();
    }
}