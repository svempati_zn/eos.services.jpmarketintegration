﻿using eos.service.JPMarketIntegration.Domain.Models.Lookup;
using EOS.Infrastructure.Common.Response;

namespace eos.service.JPMarketIntegration.application.Interfaces
{
    public interface IUtilityProcessor
    {
        List<UtilityFileConfiguration> GetAllUtilityFileConfigData();

        Response<bool> UpdateUtilityFileConfiguration(int utilityFileConfigId);
        string GetUtilityDunsByUtilityId(int? utilityId);
    }
}