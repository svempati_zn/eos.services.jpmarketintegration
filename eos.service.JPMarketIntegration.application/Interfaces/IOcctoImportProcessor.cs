﻿using eos.service.JPMarketIntegration.Domain.Models;

namespace eos.service.JPMarketIntegration.application.Interfaces
{
    public interface IOcctoImportProcessor
    {
        List<string> GetUtilityFileLogNamesByIsProcessed(int utilityId);
        int InsertUtilityFileLog(UtilityFileLogModel utilityFileLogModel);

        int InsertUtilityFileLogDetail(UtilityFileLogDetailModel utilityFileLogDetailModel);

        bool UpdateUtilityFileLog(UtilityFileLogModel utilityFileLog);
        bool UpdateUtilityFileLogDetail(UtilityFileLogDetailModel utilityFileLogDetailModel);

        string GetClientDunsByUtilityDuns(string utilityDuns);
    }
}